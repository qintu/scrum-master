import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { ChartOptions, ChartType } from 'chart.js';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Color, Label } from 'ng2-charts';
import Speech from 'speak-tts';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'scrum-master';
  speech = new Speech();
  @ViewChild('tpl', { read: ElementRef }) tpl: ElementRef;
  teammates = [];
  shuffledTeammates = [];
  parties = new Set();
  partyList = '';
  week = new Date().getDay();
  stats = new Map();

  show16 = false;
  showChart = false;
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartData: ChartDataSets[] = [];
  timestack = [];
  timeOccupiedList = []
  barChartColors: Color[] = [{ backgroundColor: '#673ab7' }];


  ngOnInit(): void {
    if (this.week === 3) {
      console.log(environment.teammates_2);
      this.teammates = environment.teammates_2;
    } else {
      console.log(environment.teammates_1);
      this.teammates = environment.teammates_1;
    }

    this.speech.init({
      volume: 1,
      lang: 'en-GB',
      rate: 1,
      pitch: 1,
      voice: 'Google UK English Male',
      splitSentences: true,
      listeners: {
        onvoiceschanged: (voices) => {
          console.log('Event voiceschanged', voices);
        },
      },
    });

    this.timeOccupiedList = Array(this.teammates.length).fill(0);
  }

  go(): void {
    this.shuffledTeammates = _.shuffle(this.teammates);
    this.text2voice(
      "Hello, how are you today? Let's start our daily meeting right now!",
    );
  }

  away(): void {
    this.text2voice('Happy hour is over but still enjoy your day!');
    this.shuffledTeammates = [];
    this.parties.clear();
    this.partyList = '';
    this.show16 = false;
    this.showChart = false;
  }

  changeStatus(event): void {
    if (event.target.checked) {
      this.parties.add(event.target.value);
    } else {
      this.parties.delete(event.target.value);
    }

    this.partyList = Array.from(this.parties).join('  ->  ');
  }

  onClick(name, index) {
    this.text2voice('Good Morning, ' + name.replace('(', 'is on'));
    this.tpl.nativeElement.children[index].style.color = 'gray';
    this.timestack.push(moment());
  }

  show16Min() {
    this.timestack.push(moment());
    if (this.partyList.length > 0) {
      this.show16 = true;
      this.text2voice("Great, let's start our 16th minute");
    } else {
      this.show16 = false;
      this.text2voice("Awesome, no blockers, no showtime");
    }
  }

  showStats() {
    this.timeOccupiedList.forEach((_, ind) => {
      const s = this.timestack[ind + 1].diff(this.timestack[ind]);
      this.timeOccupiedList[ind] = moment.duration(s).asSeconds();
    })
    this.barChartLabels = this.shuffledTeammates;
    this.barChartData = [{ data: this.timeOccupiedList, label: 'Time-Consuming (sec)' }]
    console.log(this.timeOccupiedList);
    this.showChart = true;
  }

  private text2voice(text: string) {
    this.speech
      .speak({ text: text })
      .then(() => console.log('success!'))
      .catch((e) => console.error('an error occur: ', e));
  }
}
