import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { CanvasClockComponent } from './canvas-clock/canvas-clock.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [AppComponent, CanvasClockComponent],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    ChartsModule,
    AppRoutingModule,
    MatButtonModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
